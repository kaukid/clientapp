import React, { Component } from "react";
import GigaNav from "./giganav.js";
import Routes from "./Routes";


//There is no reason to modify this file without giving me a compelling reason

export default class App extends Component {
    render() {
      return (
          <div className="appcontainer">
               <GigaNav />
               <Routes />
               
          </div>
         
      );
    }
}
//What's not to love about BB?
