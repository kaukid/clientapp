import React from 'react';
import ReactDOM from 'react-dom';
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import './index.css';
import { BrowserRouter as Router } from "react-router-dom";
import App from "./App";
import "./App.css";
import * as serviceWorker from './serviceWorker';
ReactDOM.render(
    <Router>
      <App />
    </Router>,
    document.getElementById("root")
  );
  
//There is no reason to modify this file without giving me a compelling reason

// unregister() to register() below. Please talk with me first before making any modifications
serviceWorker.unregister();
