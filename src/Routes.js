import React from "react";
import { Route, Switch } from "react-router-dom";
import Home from "./container/Home";
import LoginScreen from "./container/Login";
import AddRemove from "./container/Dgrid";

//import other components as needed Login will have routing logic that may appear or need to appear
//before the export


export default () =>
  <Switch>
    <Route path="/" exact component={Home} />
    <Route path="/login" exact component={LoginScreen} />
    <Route path="/dgrid" exact component={AddRemove} />

  </Switch>;
