import React from 'react';
import { Button, Form, FormGroup, Input } from 'reactstrap';
import { NavLink } from 'react-router-dom'
import image from '../svg/AquaKO.svg';
import './Login.css';

//This is where the call to the Routejs goes once the logic is formalized
//Once we have the backend connected, we will need to add in a failed alert



export default class LoginScreen extends React.Component {
  render() {
    return (
      <div className='wrapper fadeInDown'>
        <div className='fadeIn first' id='formContent'><Form>
        <div  className="logobox"  ><img src={ image } alt="" height="40"/></div>
      <FormGroup className='fadeIn second shim'>
       {/* <Label for="exampleEmail" className="mr-sm-2">Email</Label> */}
        <Input type="email" name="email"  id="login" placeholder="something@wow.com" />
      </FormGroup>
      <FormGroup className='fadeIn third'>
        {/* <Label for="examplePassword" className="mr-sm-2">Password</Label> */}
        <Input type="password" name="password"  id="password" placeholder="Don't Tell!" />
      </FormGroup>
      <div className="btnbox"> <NavLink to="/dgrid"><Button className='btn-block fadeIn fourth'>Submit</Button></NavLink></div>
     
    </Form></div>
    
    
    
    </div>
      
    );
  }
}
